/// results: no difference
#[macro_use]
extern crate glium;
extern crate criterion;

use std::borrow::Cow;

use criterion::{Bencher, Criterion};

fn create_texture<F>(facade: &F,
                     data: Option<Vec<f32>>,
                     width: u32,
                     height: u32)
                     -> Result<glium::texture::Texture2d, glium::texture::TextureCreationError>
    where F: glium::backend::Facade
{
    match data {
        Some(d) => {
            let data = glium::texture::RawImage2d {
                data: Cow::Owned(d),
                width: width,
                height: height,
                format: glium::texture::ClientFormat::F32,
            };
            glium::texture::Texture2d::with_format(facade,
                                                   data,
                                                   glium::texture::UncompressedFloatFormat::F32,
                                                   glium::texture::MipmapsOption::NoMipmap)
        }
        None => {
            glium::texture::Texture2d::empty_with_format(
                facade,
                glium::texture::UncompressedFloatFormat::F32,
                glium::texture::MipmapsOption::NoMipmap,
                width,
                height)
        }
    }
}

fn main() {
    use glium::{DisplayBuild, Surface};
    use glium::backend::Facade;

    let display = glium::glutin::WindowBuilder::new().with_visibility(false).build_glium().unwrap();

    let data = vec![3.0_f32; 1_000_000];

    let (width, height) = (1000, 1000);

    let data = create_texture(&display, Some(data), width, height).unwrap();

    let output = create_texture(&display, None, width, height).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 0.0],
        };
        let vertex2 = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 1.0],
        };
        let vertex3 = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 0.0],
        };
        let vertex4 = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 1.0],
        };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out vec4 color;

            uniform sampler2D tex;
            uniform float step_x;
            uniform float step_y;

            void main() {
                vec4 sum = vec4(0.0);
                for (int i = -5; i <= 5; i++) {
                    for (int j = -5; j <= 5; j++) {
                        sum += texture(tex, v_tex_coords + vec2(float(i) * step_x, float(j) * step_y));
                    }
                }
                color = sum;
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, &fragment_shader_src, None)
            .unwrap()
    };

    let uniforms_nearest = uniform! {
        tex: data.sampled()
                .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest),
        step_x: 1.0 / width as f32,
        step_y: 1.0 / height as f32
    };
    let uniforms_linear = uniform! {
        tex: data.sampled()
                .magnify_filter(glium::uniforms::MagnifySamplerFilter::Linear)
                .minify_filter(glium::uniforms::MinifySamplerFilter::Linear),
        step_x: 1.0 / width as f32,
        step_y: 1.0 / height as f32
    };

    let lookup_linear = |b: &mut Bencher| {
        b.iter(|| {
            output.as_surface()
                  .draw(&vertex_buffer,
                        &indices,
                        &program,
                        &uniforms_linear,
                        &Default::default())
                  .unwrap();
            display.get_context().finish();
        });
    };

    let lookup_nearest = |b: &mut Bencher| {
        b.iter(|| {
            output.as_surface()
                  .draw(&vertex_buffer,
                        &indices,
                        &program,
                        &uniforms_nearest,
                        &Default::default())
                  .unwrap();
            display.get_context().finish();
        });
    };


    Criterion::default()
        .sample_size(40)
        .noise_threshold(0.02)
        .bench_function("lookup_nearest", lookup_nearest)
        .bench_function("lookup_linear", lookup_linear);
}
