/// results: no difference
#[macro_use]
extern crate glium;
extern crate criterion;

use std::borrow::Cow;

use criterion::{Bencher, Criterion};

fn create_texture<F>(facade: &F,
                     data: Option<Vec<f32>>,
                     width: u32,
                     height: u32)
                     -> Result<glium::texture::Texture2d, glium::texture::TextureCreationError>
    where F: glium::backend::Facade
{
    match data {
        Some(d) => {
            let data = glium::texture::RawImage2d {
                data: Cow::Owned(d),
                width: width,
                height: height,
                format: glium::texture::ClientFormat::F32,
            };
            glium::texture::Texture2d::with_format(facade,
                                                   data,
                                                   glium::texture::UncompressedFloatFormat::F32,
                                                   glium::texture::MipmapsOption::NoMipmap)
        }
        None => {
            glium::texture::Texture2d::empty_with_format(
                facade,
                glium::texture::UncompressedFloatFormat::F32,
                glium::texture::MipmapsOption::NoMipmap,
                width,
                height)
        }
    }
}

fn main() {
    use glium::{DisplayBuild, Surface};
    use glium::backend::Facade;

    let display = glium::glutin::WindowBuilder::new().with_visibility(false).build_glium().unwrap();

    let data = vec![3.0_f32; 1_000_000];

    let (width, height) = (1000, 1000);

    let data = create_texture(&display, Some(data), width, height).unwrap();

    let output_1 = create_texture(&display, None, width, height).unwrap();
    let output_2 = create_texture(&display, None, width, height).unwrap();

    let combined_output =
        glium::texture::Texture2d::empty_with_format(&display,
                                                     glium::texture::UncompressedFloatFormat::F32F32,
                                                     glium::texture::MipmapsOption::NoMipmap,
                                                     width,
                                                     height).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 0.0],
        };
        let vertex2 = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 1.0],
        };
        let vertex3 = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 0.0],
        };
        let vertex4 = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 1.0],
        };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let vertex_shader_src = r#"
        #version 140

        in vec2 position;
        in vec2 tex_coords;
        out vec2 v_tex_coords;

        void main() {
            v_tex_coords = tex_coords;
            gl_Position = vec4(position, 0.0, 1.0);
        }
    "#;

    let program_1 = {

        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out float color;

            uniform sampler2D tex;
            uniform float step_x;
            uniform float step_y;

            void main() {
                color = texture(tex, v_tex_coords + vec2(step_x, 0.0)).r - texture(tex, v_tex_coords - vec2(step_x, 0.0)).r;
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, &fragment_shader_src, None)
            .unwrap()
    };

    let program_2 = {

        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out float color;

            uniform sampler2D tex;
            uniform float step_x;
            uniform float step_y;

            void main() {
                color = texture(tex, v_tex_coords + vec2(0.0, step_y)).r - texture(tex, v_tex_coords - vec2(0.0, step_y)).r;
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, &fragment_shader_src, None)
            .unwrap()
    };

    let program_combined = {

        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out vec2 color;

            uniform sampler2D tex;
            uniform float step_x;
            uniform float step_y;

            void main() {
                float color1 = texture(tex, v_tex_coords + vec2(step_x, 0.0)).r - texture(tex, v_tex_coords - vec2(step_x, 0.0)).r;
                float color2 = texture(tex, v_tex_coords + vec2(0.0, step_y)).r - texture(tex, v_tex_coords - vec2(0.0, step_y)).r;
                color = vec2(color1, color2);
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, &fragment_shader_src, None)
            .unwrap()
    };

    let uniforms = uniform! {
        tex: data.sampled()
                .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest),
        step_x: 1.0 / width as f32,
        step_y: 1.0 / height as f32
    };

    let run_programs_1_and_2 = |b: &mut Bencher| {
        b.iter(|| {
            output_1.as_surface()
                  .draw(&vertex_buffer,
                        &indices,
                        &program_1,
                        &uniforms,
                        &Default::default())
                  .unwrap();
            output_2.as_surface()
                  .draw(&vertex_buffer,
                        &indices,
                        &program_2,
                        &uniforms,
                        &Default::default())
                  .unwrap();
            display.get_context().finish();
        });
    };

    let run_programs_combined = |b: &mut Bencher| {
        b.iter(|| {
            combined_output.as_surface()
                  .draw(&vertex_buffer,
                        &indices,
                        &program_combined,
                        &uniforms,
                        &Default::default())
                  .unwrap();
            display.get_context().finish();
        });
    };


    Criterion::default()
        .sample_size(40)
        .noise_threshold(0.02)
        .bench_function("run_programs_1_and_2", run_programs_1_and_2)
        .bench_function("run_programs_combined", run_programs_combined);
}
